#!/hint/zsh

function source-if-exists() {
    [[ -f $1 ]] && source $1 || true
}

# p10k instant prompt.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
source-if-exists "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"

autoload -Uz colors && colors

HISTFILE=~/.zsh_history
HISTSIZE=100000
SAVEHIST=100000
REPORTTIME=10
READNULLCMD=less
DIRSRACKSIZE=100
TIMEFMT="$fg[green]%J$reset_color time: $fg[blue]%*Es$reset_color, cpu: $fg[blue]%P$reset_color"

typeset -a _path_prepend=("$HOME/.local/bin") _path_append _compinit_args

_TAG=("$(uname)")
((${+commands[nix]})) && _TAG+=("nix")
((${+commands[brew]})) && _TAG+=("brew")
[[ -f /etc/os-release ]] && _TAG+=($(source /etc/os-release && echo "$ID" "$ID_LIKE" 2>/dev/null))

if (($_TAG[(Ie)brew])); then
    _BREW_PREFIX="$(brew --prefix)"
fi

zstyle ':completion:*' completer _complete _ignored # _approximate
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}'
# zstyle ':completion:*' max-errors 1
# zstyle ':completion:*' menu auto=20 select=20
# zstyle ':completion:*' list-prompt ''
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-cache true
zstyle ':completion:*' cache-path "${XDG_CACHE_HOME:-$HOME/.cache}/zsh"
zstyle ':completion:*' insert-tab false
zstyle ':completion:*' use-ip true
zstyle ':completion:*:processes' command "ps -u $USER -o pid,comm,command -ww"
zstyle ':completion:*:*:docker:*' option-stacking yes
zstyle ':completion:*:*:docker-*:*' option-stacking yes
zstyle ':compinstall' filename "$HOME/.zshrc"

setopt auto_continue
setopt auto_pushd
setopt bashautolist
setopt check_jobs
setopt check_running_jobs
setopt extended_history
setopt hist_fcntl_lock
setopt hist_find_no_dups
setopt hist_ignore_all_dups
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_no_functions
setopt hist_reduce_blanks
setopt hist_save_no_dups
setopt hist_verify
setopt inc_append_history
setopt interactive_comments
setopt listpacked
setopt magic_equal_subst
setopt noautomenu
setopt nobeep
setopt nohashdirs
setopt nohup
setopt nomatch
setopt prompt_subst
setopt pushd_ignore_dups
setopt pushd_minus
setopt pushd_silent
setopt pushd_to_home
setopt rm_star_silent
setopt transient_rprompt

# aliases
alias ls='ls -vb -CF --color=auto'
alias ll='ls -lh'
alias la='ll -a'
alias fzl='fzf --tac +s'
alias grep='grep --color=auto'
alias rename='perl-rename'
alias mtr='sudo mtr'
alias proxychains='proxychains '
alias cdtemp='cd $(mktemp -d)'
alias sc='systemctl'
alias gist='gist -p --skip-empty'
alias gdb='gdb -q'
alias dh='dirs -v'
alias sudo='sudo '

# typo fix
alias ivm=vim
alias vm=vim
alias sl=ls
alias LS=ls
alias pign=ping

if (($_TAG[(Ie)Linux])); then
    ((${+commands[journalctl]})) && alias tailfmsg='journalctl -a -f'

    # colorize
    if ((${+commands[grc]})); then
        # alias dig='grc --pty dig'
        alias df='grc df'
        # alias docker='grc --pty docker'
        alias ip='grc ip'
        alias mount='grc mount'
        alias ss='grc ss'
    fi

    if ((${+commands[ccache]})); then
        export USE_CCACHE=1
        _path_prepend+=("/usr/lib/ccache/bin")
    fi

    if ((${+commands[flatpak]})); then
        _path_prepend+=("$HOME/.local/share/flatpak/exports/bin")
    fi

    if ((${+commands[wine]})); then
        function activate-wine-prefix() {
            local prefix="${1:-$PWD}"

            while [[ $prefix != "/" ]]; do
                if [[ -d "$prefix/dosdevices" && -f "$prefix/.update-timestamp" ]]; then
                    export WINEPREFIX="$prefix"
                    echo "Activated Wine prefix at $WINEPREFIX"
                    return
                fi
                prefix="$prefix/.."
                prefix=${prefix:A}
            done

            echo "Unable to find a Wine prefix"
        }
    fi

    if (($_TAG[(Ie)arch])); then
        _pacman=""
        if ((${+commands[paru]})); then
            _pacman="paru"
        elif ((${+commands[pikaur]})); then
            _pacman="pikaur"
        else
            _pacman="sudo pacman"
        fi

        if [[ -n $_pacman ]]; then
            alias felix="$_pacman -Syu --noconfirm"
        fi
        unset _pacman

        function mksrcinfo() {
            makepkg --printsrcinfo > .SRCINFO
        }

        source-if-exists /usr/share/doc/pkgfile/command-not-found.zsh
    fi

    if (($_TAG[(Ie)fedora])); then
        alias felix="sudo dnf distro-sync --refresh --assumeyes"

        fpath=(/usr/share/zsh/vendor-completions $fpath)

        source-if-exists /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
    fi

    # naughty profiles that have functions inside
    source-if-exists /etc/profile.d/vte.sh
    source-if-exists /etc/profile.d/wezterm.sh

    source-if-exists /usr/share/fzf/completion.zsh
    source-if-exists /usr/share/fzf/key-bindings.zsh
    source-if-exists /usr/share/fzf/shell/key-bindings.zsh
    source-if-exists /usr/share/zsh/site-functions/zsh-autosuggestions.zsh
    source-if-exists /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
fi

if (($_TAG[(Ie)Darwin])); then
    if (($_TAG[(Ie)nix])); then
        fpath=(~/.nix-profile/share/zsh/site-functions $fpath)

        source-if-exists ~/.nix-profile/share/fzf/completion.zsh
        source-if-exists ~/.nix-profile/share/fzf/key-bindings.zsh
        source-if-exists ~/.nix-profile/share/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
        source-if-exists ~/.nix-profile/share/zsh/plugins/nix/nix.plugin.zsh
    fi

    if (($_TAG[(Ie)brew])); then
        # fix compaudit error with brew
        _compinit_args=(-u)

        _path_append+=("$_BREW_PREFIX/sbin")
    fi

    setopt nullglob
    for d in "$HOME/Library/Python/"*; do
        _path_prepend+=("$d/bin")
    done
    unsetopt nullglob

    # fix locale
    export LANG=en_US.UTF-8
    export LC_ALL=en_US.UTF-8
fi

if ((${+commands[go]})); then
    export GOPATH="${HOME}/.go"
    _path_prepend+=("$GOPATH/bin")
fi

if ((${+commands[node]})); then
    _NPM_PACKAGES="$HOME/.local/lib/nodejs"
    _path_append+=("$_NPM_PACKAGES/bin")
    export MANPATH="$_NPM_PACKAGES/share/man:$(manpath 2>/dev/null)"
    unset _NPM_PACKAGES
fi

typeset -U path=($_path_prepend $path $_path_append)
typeset -U fpath
unset _path_prepend _path_append

((${+commands[dircolors]})) && eval "$(dircolors)"
((${+commands[lesspipe.sh]})) && eval "$(lesspipe.sh)"

# complete list with colors
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

typeset -gaU precmd_functions

# set title
_precmd_set_title() { print -Pn "\e]0;zsh: %n %~\a" }
precmd_functions+=_precmd_set_title

export LESS="-iRF"

function man() {
    LESS="" \
        LESS_TERMCAP_mb="$(printf "\e[5m")" \
        LESS_TERMCAP_md="$(printf "\e[1;31m")" \
        LESS_TERMCAP_me="$(printf "\e[0m")" \
        LESS_TERMCAP_se="$(printf "\e[0m")" \
        LESS_TERMCAP_so="$(printf "\e[1;44;33m")" \
        LESS_TERMCAP_ue="$(printf "\e[0m")" \
        LESS_TERMCAP_us="$(printf "\e[1;36;4m")" \
        command man "$@"
}

source-if-exists ~/.local/share/zsh/plugins/powerlevel10k/powerlevel10k.zsh-theme
source-if-exists ~/.p10k.zsh
source-if-exists ~/.local/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# set up completion
zmodload zsh/complist
autoload -Uz edit-command-line
autoload -Uz compinit
compinit "${_compinit_args[@]}"

# keybindings
bindkey -e

zle -N edit-command-line
bindkey '^X^E' edit-command-line

# Tab should not expand variables
bindkey '^I' complete-word

# Force path name completion
bindkey "^[/" complete-path
zle -C complete-path complete-word _generic
zstyle ':completion:complete-path:*' completer _files

# Force menu complete
bindkey "^[m" force-menu-complete
zle -C force-menu-complete menu-select _generic

# delete word until slash
# https://www.zsh.org/mla/users/2001/msg00870.html
tcsh-backward-delete-word () {
  local WORDCHARS="${WORDCHARS:s#/#}"
  zle backward-delete-word
}
zle -N tcsh-backward-delete-word
bindkey '^W' tcsh-backward-delete-word

# Home/End/Delete/Arrow keys {{{1

# create a zkbd compatible hash
# to add other keys to this hash, see: man 5 terminfo
typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[ShiftTab]="${terminfo[kcbt]}"
key[CtrlUp]="${terminfo[kUP5]}"
key[CtrlDown]="${terminfo[kDN5]}"
key[CtrlLeft]="${terminfo[kLFT5]}"
key[CtrlRight]="${terminfo[kRIT5]}"

# setup key accordingly
[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"      beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"       end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"    overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}" backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"    delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"        up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"      down-line-or-history
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"      backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"     forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"    beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"  end-of-buffer-or-history
[[ -n "${key[ShiftTab]}"  ]] && bindkey -- "${key[ShiftTab]}"  reverse-menu-complete
[[ -n "${key[CtrlLeft]}"  ]] && bindkey -- "${key[CtrlLeft]}"  backward-word
[[ -n "${key[CtrlRight]}" ]] && bindkey -- "${key[CtrlRight]}" forward-word

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
    autoload -Uz add-zle-hook-widget
    function zle_application_mode_start {
        echoti smkx
    }
    function zle_application_mode_stop {
        echoti rmkx
    }
    add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
    add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi
# }}}


typeset -A ZSH_HIGHLIGHT_STYLES
typeset -A _ZSH_HIGHLIGHT_STYLES_OVERRIDE

# rough translation of fast-syntax-highlighting clean theme
_ZSH_HIGHLIGHT_STYLES_OVERRIDE=(
    [unknown-token]="fg=124,bold"
    [reserved-word]="fg=146"
    [alias]="fg=109"
    [suffix-alias]="fg=109"
    [global-alias]="bg=19"
    [builtin]="fg=109"
    [function]="fg=109"
    [command]="fg=109"
    [precommand]="fg=109"
    [commandseparator]=""
    [hashed-command]="fg=109"
    [autodirectory]="fg=208,underline"
    [path]="fg=208"
    [path_prefix]="fg=208"
    [globbing]="fg=220"
    [history-expansion]="fg=blue,bold"
    [arithmetic-expansion]="fg=146"
    [single-hyphen-option]="fg=185"
    [double-hyphen-option]="fg=185"
    [single-quoted-argument]="fg=147"
    [double-quoted-argument]="fg=147"
    [dollar-quoted-argument]="fg=147"
)

for k v in "${(@kv)_ZSH_HIGHLIGHT_STYLES_OVERRIDE}"; do
    ZSH_HIGHLIGHT_STYLES[$k]="$v"
done

source-if-exists ~/.zshrc.local

unset _TAG
