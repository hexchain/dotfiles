-- Additional file type detection
vim.api.nvim_create_augroup('filetypedetect', {clear = false})
vim.api.nvim_create_autocmd({'BufRead', 'BufNewFile'}, {
    pattern = 'PKGBUILD',
    callback = function()
        vim.bo.filetype = 'sh'
    end,
    group = 'filetypedetect',
})
vim.filetype.add({
    extension = {
        service = "dosini",
    },
    filename = {
        ["PKGBUILD"] = "sh",
    },
})

-- Additional file-type-specific options
vim.api.nvim_create_augroup('filetypeoptions', {clear = false})
vim.api.nvim_create_autocmd('FileType', {
    pattern = 'python',
    callback = function()
        vim.bo.shiftwidth = 4
        vim.bo.softtabstop = 4
        vim.wo.colorcolumn = '88'
    end,
    group = 'filetypeoptions',
})
vim.api.nvim_create_autocmd('FileType', {
    pattern = 'go',
    callback = function()
        vim.bo.shiftwidth = 8
        vim.bo.tabstop = 8
        vim.bo.expandtab = false
    end,
    group = 'filetypeoptions',
})
vim.api.nvim_create_autocmd('FileType', {
    pattern = {'json', 'yaml', 'rst'},
    callback = function()
        vim.bo.shiftwidth = 2
        vim.bo.softtabstop = 2
    end,
    group = 'filetypeoptions',
})
vim.api.nvim_create_autocmd('FileType', {
    pattern = {'gitcommit', 'patch'},
    callback = function()
        vim.wo.list = false
    end,
    group = 'filetypeoptions',
})
vim.api.nvim_create_autocmd('FileType', {
    pattern = 'makefile',
    callback = function()
        vim.bo.expandtab = false
    end,
    group = 'filetypeoptions',
})
