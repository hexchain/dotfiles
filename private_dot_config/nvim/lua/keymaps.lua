local utils = require('utils')
map = utils.map
nmap = utils.nmap

vim.g.mapleader = '\\'

local M = {}

nmap('<leader><space>', '<cmd>nohl<cr>')

nmap('<space>', 'za')

-- Disable arrow keys
-- map('', '<Up>', '<nop>')
-- map('', '<Down>', '<nop>')
-- map('', '<Left>', '<nop>')
-- map('', '<Right>', '<nop>')

-- Diagnostics
nmap('[d', vim.diagnostic.goto_prev)
nmap(']d', vim.diagnostic.goto_next)
