local conf = require('plugins')
return {
    -- Fix CursorHold event
    {'antoinemadec/FixCursorHold.nvim'},

    -- recognize editorconfig
    {'editorconfig/editorconfig-vim'},

    -- try to be smarter for indent
    {'tpope/vim-sleuth'},

    -- colorscheme
    {'folke/tokyonight.nvim', config = conf.tokyonight},

    -- status line
    {'nvim-lualine/lualine.nvim', config = conf.lualine},

    -- diagnostic panel
    {'folke/trouble.nvim', config = conf.trouble},

    -- which-key
    {'folke/which-key.nvim', config = true},

    -- show search result position and count
    {'google/vim-searchindex'},

    -- auto pair
    {'windwp/nvim-autopairs', config = true},

    -- completion
    {
        'hrsh7th/nvim-cmp',
        dependencies = {
            {'hrsh7th/cmp-buffer'},
            {'hrsh7th/cmp-path'},
            {'hrsh7th/cmp-nvim-lsp-signature-help'},
            {'hrsh7th/cmp-cmdline'},
            {'hrsh7th/cmp-nvim-lua'},
            {'hrsh7th/cmp-emoji'},
            {'hrsh7th/cmp-nvim-lsp'},
            {'hrsh7th/cmp-vsnip'},
        },
        config = conf.cmp,
    },
    {'hrsh7th/vim-vsnip'},

    -- language server config
    {'neovim/nvim-lspconfig', config = conf.lspconfig},

    -- show LSP diagnostics as virtual lines
    {'https://git.sr.ht/~whynothugo/lsp_lines.nvim', config = conf.lsp_lines},

    -- symbols outline
    {'simrat39/symbols-outline.nvim', config = conf.symbols_outline},

    --- lsp status
    {'nvim-lua/lsp-status.nvim', config = conf.lsp_status},

    -- treesitter
    {'nvim-treesitter/nvim-treesitter', config = conf.treesitter},
    {'nvim-treesitter/nvim-treesitter-textobjects'},
    {'romgrk/nvim-treesitter-context', name = 'treesitter-context', config = true},

    -- show code action
    {'kosayoda/nvim-lightbulb', config = conf.nvim_lightbulb},

    -- telescope
    {
        'nvim-telescope/telescope.nvim', branch = '0.1.x',
        dependencies = {'nvim-lua/plenary.nvim'},
        config = conf.telescope,
    },
    {
        'nvim-telescope/telescope-file-browser.nvim',
        dependencies = {
            'nvim-telescope/telescope.nvim',
        },
    },

    {'ahmedkhalf/project.nvim', config = conf.project_nvim},

    {
        'nvim-neo-tree/neo-tree.nvim',
        branch = 'v2.x',
        dependencies = {
            'nvim-lua/plenary.nvim',
            'MunifTanjim/nui.nvim',
            'kyazdani42/nvim-web-devicons',
        },
        config = conf.neo_tree,
    },

    {'rmagatti/auto-session', config = true},
    {
        'rmagatti/session-lens',
        dependencies = {
            'rmagatti/auto-session',
        },
        config = true,
    },

    {'lewis6991/gitsigns.nvim', config = true},
    {'lukas-reineke/indent-blankline.nvim', config = conf.indent_blankline},
    {'terrortylor/nvim-comment', name = 'nvim_comment', config = true},
    {'stevearc/dressing.nvim', config = 'true'},
    {'tpope/vim-eunuch', cmd = {'SudoEdit', 'SudoWrite', 'Remove', 'Rename', 'Delete', 'Move', 'Unlink'}},
    {'nfnty/vim-nftables'},
    {'tpope/vim-fugitive'},
    {'tpope/vim-surround'},
    {'google/vim-jsonnet'},
    {'grafana/vim-alloy'},
    {'jamessan/vim-gnupg', config = function()
        vim.g.GPGUsePipes = 1
    end},
}
