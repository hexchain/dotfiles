local M = {}

function M.indent_blankline()
    require('ibl').setup()
end

function M.lualine()
    local lsp_status = require('lsp-status')
    local function lsp_current_function()
        local cfn = vim.b.lsp_current_function
        if cfn then
            return cfn
        end
        return ''
    end
    require('lualine').setup {
        options = {
            disabled_filetypes = {
                statusline = {'neo-tree'}
            }
        },
        sections = {
            lualine_c = {'filename', lsp_current_function},
            lualine_x = {lsp_status.status_progress, 'encoding', 'fileformat', 'filetype'}
        }
    }
end

function M.telescope()
    local telescope = require('telescope')
    telescope.setup {
        extensions = {
            ['file_browser'] = {
                theme = 'ivy',
            }
        }
    }

    telescope.load_extension 'projects'
    telescope.load_extension 'file_browser'

    local builtin = require('telescope.builtin')
    vim.keymap.set('n', '<leader>ff', builtin.find_files, {desc = 'Find files'})
    vim.keymap.set('n', '<leader>fb', telescope.extensions.file_browser.file_browser, {desc = 'Browse files'})
    vim.keymap.set('n', '<leader>a', builtin.live_grep, {desc = 'Search in files'})
    vim.keymap.set('n', '<leader>A', builtin.grep_string, {desc = 'Search current word in files'})
    vim.keymap.set('n', '<leader>o', builtin.pickers, {desc = 'Last picker'})
    vim.keymap.set('n', '<leader>p', builtin.resume, {desc = 'Recent pickers'})
end

function M.treesitter()
    local treesitter = require('nvim-treesitter.configs')
    treesitter.setup {
        auto_install = true,
        highlight = {enable = true},
        incremental_selection = {enable = true},
        indent = {enable = true},
        textobjects = {
            move = {
                enable = true,
                set_jumps = true,
                goto_next_start = {
                    [']m'] = '@class.outer',
                    [']]'] = '@function.outer',
                },
                goto_next_end = {
                    [']M'] = '@class.outer',
                },
                goto_previous_start = {
                    ['[m'] = '@class.outer',
                    ['[['] = '@function.outer',
                },
                goto_previous_end = {
                    ['[M'] = '@class.outer',
                },
            },
            select = {
                enable = true,
                lookahead = true,
                keymaps = {
                    ['af'] = '@function.outer',
                    ['if'] = '@function.inner',
                    ['ac'] = '@class.outer',
                    ['ic'] = '@class.inner',
                },
            },
        },
    }
end

function M.lspconfig()
    local nvim_lsp = require('lspconfig')
    local lsp_status = require('lsp-status')
    local capabilities = vim.tbl_extend('keep',
        require('cmp_nvim_lsp').default_capabilities(),
        lsp_status.capabilities)

    vim.api.nvim_create_autocmd('LspAttach', {
        group = vim.api.nvim_create_augroup('UserLspConfig', {}),
        callback = function (ev)
            -- Mappings
            -- See `:help vim.lsp.*` for documentation on any of the below functions
            local trouble = require('trouble')
            local telescope_builtin = require('telescope.builtin')

            local opts = function(desc)
                return {noremap = true, silent = true, buffer = ev.buf, desc = desc}
            end
            vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts('Hover'))
            vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts('Signature help'))
            vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts('Declaration'))
            vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts('Definition'))
            vim.keymap.set('n', 'gr', function() trouble.toggle({mode = 'lsp_references'}) end, opts('References'))
            vim.keymap.set('n', 'gi', function() trouble.toggle({mode = 'lsp_implementations'}) end, opts('Implementations'))
            vim.keymap.set('n', 'gI', telescope_builtin.lsp_incoming_calls, opts('Incoming calls'))
            vim.keymap.set('n', 'gO', telescope_builtin.lsp_outgoing_calls, opts('Outgoing calls'))
            vim.keymap.set('n', '<leader>c', vim.lsp.buf.code_action, opts('Code action'))
            vim.keymap.set('n', '<leader>=', function () vim.lsp.buf.format { async = true } end, opts('Format with LSP'))
            vim.keymap.set('n', '<leader>R', vim.lsp.buf.rename, opts('Rename'))
            vim.keymap.set('n', '<leader>t', telescope_builtin.lsp_document_symbols, opts('Document symbols'))
            vim.keymap.set('n', '<leader>wa', vim.lsp.buf.add_workspace_folder, opts('Add workspace folder'))
            vim.keymap.set('n', '<leader>wr', vim.lsp.buf.remove_workspace_folder, opts('Remove workspace folder'))
            vim.keymap.set('n', '<leader>wl', function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end, opts('List workspace folders'))
            vim.keymap.set({'n', 'i'}, '<M-Return>', vim.lsp.buf.code_action, opts('Code action'))
            vim.keymap.set({'n', 'i'}, '<M-L>', function () vim.lsp.buf.format { async = true } end, opts('Format'))
        end
    })

    local servers = {
        'bashls',
        'ccls',
        'clangd',
        'gopls',
        'pylsp',
        'pyright',
        'ts_ls',
        'yamlls',
        'lua_ls',
    }
    for k, v in pairs(servers) do
        local lsp
        local settings = {
            on_attach = function (client, _)
                lsp_status.on_attach(client)
            end,
            capabilities = capabilities,
            flags = {
                debounce_text_changes = 150,
            }
        }
        if type(k) == "number" then
            lsp = v
        elseif type(k) == "string" and type(v) == "table" then
            lsp = k
            settings = vim.tbl_deep_extend('force', settings, v)
        else
            goto skip
        end
        nvim_lsp[lsp].setup(settings)
        ::skip::
    end

end

function M.cmp()
    local cmp = require('cmp')

    local feedkey = function(key, mode)
        vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(key, true, true, true), mode, true)
    end

    cmp.setup {
        preselect = cmp.PreselectMode.None,
        formatting = {
            format = function(entry, vim_item)
                vim_item.menu = ({
                    buffer = '[Buffer]',
                    nvim_lsp = '[LSP]',
                    vsnip = '[VSnip]',
                    nvim_lua = '[Lua]',
                    cmp_tabnine = '[TabNine]',
                    path = '[Path]',
                    emoji = '[Emoji]'
                })[entry.source.name]
                return vim_item
            end
        },
        mapping = cmp.mapping.preset.insert({
            ['<C-d>'] = cmp.mapping.scroll_docs(-4),
            ['<C-f>'] = cmp.mapping.scroll_docs(4),
            ['<C-X><C-O>'] = cmp.mapping.complete(),
            ['<C-e>'] = cmp.mapping.close(),
            ['<C-n>'] = {
                i = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
            },
            ['<C-p>'] = {
                i = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }),
            },
            ['<CR>'] = function(fallback)
                if cmp.visible() and cmp.get_selected_entry() then
                    cmp.confirm()
                else
                    fallback()
                end
            end,
            -- https://github.com/hrsh7th/nvim-cmp/wiki/Example-mappings#intellij-like-mapping
            ['<Tab>'] = cmp.mapping(function(fallback)
                if cmp.visible() then
                    if not cmp.get_selected_entry() then
                        cmp.select_next_item({behavior = cmp.SelectBehavior.Select})
                    end
                    cmp.confirm()
                elseif vim.fn['vsnip#available'](1) == 1 then
                    feedkey('<Plug>(vsnip-expand-or-jump)', '')
                else
                    fallback()
                end
            end, {'i', 's'}),
            ['<S-Tab>'] = cmp.mapping(function(fallback)
                if cmp.visible() then
                    cmp.select_prev_item({behavior = cmp.SelectBehavior.Select})
                elseif vim.fn['vsnip#available'](-1) == 1 then
                    feedkey('<Plug>(vsnip-jump-prev)', '')
                else
                    fallback()
                end
            end, {'i', 's'}),
        }),
        snippet = {
            expand = function(args)
                vim.fn['vsnip#anonymous'](args.body)
            end,
        },
        sources = cmp.config.sources({
            {name = 'nvim_lsp'},
            {name = 'vsnip'},
            {name = 'nvim_lua'},
            {name = 'nvim_lsp_signature_help'},
            {name = 'buffer'},
            {name = 'emoji'},
        }),
    }
    cmp.setup.cmdline('/', {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources({
            {name = 'buffer'}
        })
    })
    cmp.setup.cmdline(':', {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources(
            {{name = 'path'}},
            {{name = 'cmdline'}})
    })

    vim.opt.completeopt = 'menu,menuone,noselect'
    local cmp_autopairs = require('nvim-autopairs.completion.cmp')
    cmp.event:on('confirm_done', cmp_autopairs.on_confirm_done())
end

function M.trouble()
    local trouble = require('trouble')
    trouble.setup {
        indent_lines = false,
        signs = {
            error = '🔴',
            warning = '🟡',
            hint = '🔵',
            information = '🟢'
        },
        use_diagnostic_signs = false,
    }

    vim.keymap.set('n', '<leader>d', function() trouble.toggle({mode = 'document_diagnostics'}) end, {desc = 'Show diagnostics'})
    vim.keymap.set('n', '<leader>D', function() trouble.toggle({mode = 'workspace_diagnostics'}) end, {desc = 'Show workspace diagnostics'})
end

function M.nvim_lightbulb()
    vim.api.nvim_create_augroup('lightbulb', {})
    vim.api.nvim_create_autocmd({'CursorHold', 'CursorHoldI'}, {
        pattern = '*',
        callback = function()
            require('nvim-lightbulb').update_lightbulb()
        end,
        group = 'lightbulb',
    })
end

function M.lsp_lines()
    local lsp_lines = require('lsp_lines')
    lsp_lines.setup()
    vim.diagnostic.config {
        virtual_lines = false
    }
    vim.keymap.set('', '<Leader>l', lsp_lines.toggle, {desc = 'Toggle diagnostics'})
end

function M.project_nvim()
    local project_nvim = require('project_nvim')
    project_nvim.setup {
        manual_mode = true,
        patterns = {
            '.git', '_darcs', '.hg', '.bzr', '.svn',
            'Makefile', 'package.json', 'pyproject.toml', 'setup.cfg',
            'go.mod',
            '>.config',
        },
    }
end

function M.neo_tree()
    vim.g.neo_tree_remove_legacy_commands = 1
    local neo_tree = require('neo-tree')
    neo_tree.setup {
        close_if_last_window = true,
        enable_git_status = true,
        enable_diagnostics = true,
        popup_border_style = 'single',
        filesystem = {
            hijack_netrw_behavior = "open_default",
        },
        buffers = {
            follow_current_file = true,
            group_empty_dirs = true,
            show_unloaded = true,
            window = {
                mappings = {
                    ['d'] = 'buffer_delete',
                }
            }
        }
    }
    vim.keymap.set('', '<Leader>|', ':Neotree reveal<cr>', {remap = false, silent = true})
    vim.keymap.set('', '<Leader>ft', ':Neotree toggle<cr>', {remap = false, silent = true})
    vim.keymap.set('', '<Leader>b', ':Neotree toggle show buffers right<cr>', {remap = false, silent = true})
end

function M.tokyonight()
    require('tokyonight').setup {
        style = 'storm',
        transparent = true,
        styles = {
            sidebars = "transparent",
        }
    }
    pcall(vim.cmd, "colorscheme tokyonight")
end

function M.symbols_outline()
    require('symbols-outline').setup()
    vim.keymap.set('', '<Leader>s', ':SymbolsOutline<cr>')
end

function M.lsp_status()
    local lsp_status = require('lsp-status')
    lsp_status.register_progress()
end

return M
