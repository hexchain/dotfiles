local M = {}

function M.prequire(...)
    local status, lib = pcall(require, ...)
    if status then return lib end
    return nil
end

M.map = vim.keymap.set

function M.nmap(...)
    return map('n', ...)
end

return M
