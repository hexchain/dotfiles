local g = vim.g
local o = vim.opt

o.mouse = ''
o.backup = true
o.backupdir = os.getenv('HOME') .. '/.cache/nvim/backup//'
o.directory = os.getenv('HOME') .. '/.cache/nvim/swap//'
o.undofile = true
o.undodir = os.getenv('HOME') .. '/.cache/nvim/undo//'

o.number = true
o.showmatch = true
o.linebreak = true
o.signcolumn = "yes"
o.termguicolors = true
o.inccommand = 'split'
o.conceallevel = 0
o.ignorecase = true
o.smartcase = true
o.list = true
o.listchars = "tab:>-,trail:-"
o.cursorline = true
o.wildmode = 'longest,full'
o.timeoutlen = 500
o.foldmethod = 'expr'
o.foldexpr = 'nvim_treesitter#foldexpr()'
o.foldlevelstart = 99
o.scrolloff = 5
g.cursorhold_updatetime = 500
o.sessionoptions:append 'winpos,terminal,folds'

o.softtabstop = 4
o.shiftwidth = 4
o.expandtab = true
o.smarttab = true
o.autoindent = true
o.cindent = true

o.hidden = true
o.lazyredraw = true
o.shortmess:append "sI"

vim.cmd "filetype plugin indent on"

vim.diagnostic.config {
    virtual_text = false,
}

-- Restore cursor to the previous location
vim.api.nvim_create_augroup('restore_cursor', {})
vim.api.nvim_create_autocmd('BufRead', {
    pattern = '*',
    command = [[call setpos(".", getpos("'\""))]],
    group = 'restore_cursor',
})
