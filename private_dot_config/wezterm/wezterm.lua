local w = require 'wezterm'
local act = w.action

local function font_with_default_fallback(name, params)
    local names = {
        name,
        'Noto Sans CJK SC',
        'Noto Sans CJK TC',
        'Noto Sans CJK JP',
        'Twemoji',
    }
    return w.font_with_fallback(names, params)
end

local function basename(s)
    return string.gsub(s, '(.*[/\\])(.*)', '%2')
end

local function ends_with(str, ending)
    return ending == "" or str:sub(-#ending) == ending
end

w.on('update-right-status', function(window, _)
    local time = w.time.now()
    local date = time:format "%Y-%m-%d %H:%M"

    window:set_right_status(w.format {
        { Background = { Color = 'white' } },
        { Foreground = { Color = 'black' } },
        { Text = ' ' .. utf8.char(0x1f553) .. ' ' },
        { Text = date },
        { Text = ' ' }
    })
end)

w.on('format-tab-title', function(tab, tabs, panes, config, hover, max_width)
    local pane = tab.active_pane
    local title = pane.title
    local proc_name = basename(pane.foreground_process_name)
    local title_prefix = '' .. (1 + tab.tab_index)
    if proc_name ~= "" then
        if ends_with(proc_name, "(deleted)") then
            proc_name = proc_name:sub(0, -11) .. '*'
        end
        title = proc_name
    end

    if pane.is_zoomed then
        title_prefix = title_prefix .. 'Z'
    end

    local full_title = title_prefix .. ':' .. title

    max_width = max_width - 2  -- leading and trailing space
    if w.column_width(full_title) > max_width then
        full_title = w.truncate_right(full_title, max_width - 1) .. '…'
    end

    return {
        { Text = " " },
        { Text = full_title },
        { Text = " " },
    }
end)

local keys = {
    {
        mods = 'ALT|SHIFT', key = '_',
        action = act.SplitVertical { domain = 'CurrentPaneDomain' }
    },
    {
        mods = 'ALT|SHIFT', key = '|',
        action = act.SplitHorizontal { domain = 'CurrentPaneDomain' }
    },
    {
        mods = 'ALT|SHIFT', key = 'h',
        action = act.ActivatePaneDirection 'Left',
    },
    {
        mods = 'ALT|SHIFT', key = 'l',
        action = act.ActivatePaneDirection 'Right',
    },
    {
        mods = 'ALT|SHIFT', key = 'k',
        action = act.ActivatePaneDirection 'Up',
    },
    {
        mods = 'ALT|SHIFT', key = 'j',
        action = act.ActivatePaneDirection 'Down',
    },
    {
        mods = 'ALT', key = 'z',
        action = act.TogglePaneZoomState,
    },
    {
        mods = 'ALT', key = 't',
        action = act.SpawnTab 'CurrentPaneDomain',
    },
    {
        mods = 'ALT', key = 'h',
        action = act.ScrollToPrompt(-1),
    },
    {
        mods = 'ALT', key = 'l',
        action = act.ScrollToPrompt(1),
    },
    {
        mods = 'ALT', key = 'k',
        action = act.ScrollByLine(-1),
    },
    {
        mods = 'ALT', key = 'j',
        action = act.ScrollByLine(1),
    },
}

for i = 1, 9 do
    table.insert(keys, {
        mods = 'ALT', key = tostring(i),
        action = act.ActivateTab(i - 1)
    })
end

local config = {
    enable_wayland = true,

    window_padding = { left = '2pt', right = 0, top = 0, bottom = 0 },
    use_resize_increments = true,

    use_fancy_tab_bar = false,
    hide_tab_bar_if_only_one_tab = false,
    tab_max_width = 22,

    color_scheme = 'Argonaut',
    colors = {
        background = 'black',
        selection_fg = 'black',
        selection_bg = '#fffacd',
        tab_bar = {
            background = 'black',
            active_tab = {
                bg_color = '#008df8',
                fg_color = 'white',
                intensity = 'Bold',
            },
            inactive_tab = {
                bg_color = 'black',
                fg_color = 'grey',
                intensity = 'Half',
            },
            new_tab = {
                bg_color = 'black',
                fg_color = 'white',
            },
        },
    },
    force_reverse_video_cursor = true,
    window_background_opacity = 0.85,
    inactive_pane_hsb = {
        saturation = 0.9,
        brightness = 0.5,
    },

    font = font_with_default_fallback 'JetBrains Mono NL',
    font_rules = {
        {
            intensity = 'Bold',
            font = font_with_default_fallback('JetBrains Mono NL', { weight = 'Bold' })
        },
    },
    font_size = 9,
    warn_about_missing_glyphs = false,

    visual_bell = {
        fade_in_duration_ms = 75,
        fade_out_duration_ms = 75,
        target = 'CursorColor',
    },

    scrollback_lines = 10000,
    alternate_buffer_wheel_scroll_speed = 1,
    keys = keys,
    mouse_bindings = {
        {
            event = { Up = { streak = 1, button = 'Left' } },
            mods = 'CTRL',
            action = act.OpenLinkAtMouseCursor,
        },
        {
            event = { Down = { streak = 1, button = 'Left' } },
            mods = 'CTRL',
            action = act.Nop,
        },
        {
            event = { Down = { streak = 2, button = 'Right' } },
            action = act.SelectTextAtMouseCursor 'SemanticZone',
        },
        {
            event = { Up = { streak = 2, button = 'Right' } },
            action = act.CompleteSelection 'PrimarySelection',
        },
        {
            event = { Down = { streak = 2, button = 'Left' } },
            mods = 'CTRL',
            action = act.SelectTextAtMouseCursor 'SemanticZone',
        },
        {
            event = { Up = { streak = 2, button = 'Left' } },
            mods = 'CTRL',
            action = act.CompleteSelection 'PrimarySelection',
        },
        {
            event = { Up = { streak = 1, button = 'Left' } },
            action = act.CompleteSelection 'PrimarySelection',
        },
        {
            event = { Up = { streak = 2, button = 'Left' } },
            action = act.CompleteSelection 'PrimarySelection',
        },
        {
            event = { Up = { streak = 3, button = 'Left' } },
            action = act.CompleteSelection 'PrimarySelection',
        },
    },
}

-- scroll speed fix on wayland
if os.getenv('WAYLAND_DISPLAY') ~= nil then
    config.xcursor_theme = 'breeze_cursors'
    table.insert(config.mouse_bindings, {
        event = { Down = { streak = 1, button = { WheelUp = 1 } } },
        alt_screen = false,
        action = act.ScrollByLine(-3),
    })
    table.insert(config.mouse_bindings, {
        event = { Down = { streak = 1, button = { WheelDown = 1 } } },
        alt_screen = false,
        action = act.ScrollByLine(3),
    })
end

local _config = w.config_builder()
for k, v in pairs(config) do
    _config[k] = v
end

return _config
