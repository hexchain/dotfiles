-- vim: ft=lua:

local lfs = require 'lfs';

local net_base = "/sys/class/net"
local wlanif = "wlp1s0"
local ethif = "eno1"

for dir in lfs.dir(net_base) do
    if string.match(dir, '^wl') ~= nil then
        wlanif = dir
    elseif string.match(dir, '^en') ~= nil then
        ethif = dir
    end
end

conky.config = {
    alignment = 'top_right',
    default_color = '#eeeeee',
    double_buffer = true,
    draw_borders = false,
    draw_graph_borders = true,
    draw_outline = false,
    draw_shades = false,
    gap_x = 40,
    gap_y = 40,
    if_up_strictness = 'link',
    minimum_width = 300, minimum_height = 0,
    override_utf8_locale = true,
    own_window = true,
    -- own_window_type = 'desktop',
    own_window_hints = 'undecorated,below,skip_taskbar,skip_pager,sticky',
    own_window_colour = '#000000',
    own_window_argb_visual = true,
    own_window_argb_value = 150,
    text_buffer_size = 4096,
    update_interval = 2,
    update_interval_on_battery = 4,
    use_spacer = 'none',
    use_xft = true,
    font = 'Sans:size=10',
    color0 = '#11ecf7',
    color1 = '#00c2ff',
    color2 = '#28f428',
    default_bar_height = 10,
    default_bar_width = 120,
    diskio_avg_samples = 2,
    out_to_wayland = true,
    out_to_x = false,
};

local text = [[
${color0}System ${hr 3}$color
${color1}Hostname$color $alignr${no_update $nodename}
${color1}Kernel$color $alignr${no_update $kernel}
${cpugraph cpu0 30,300 00c2ff 11ecf7}
${color1}CPU$color $alignr${cpu cpu0}% @ ${freq cpu0} MHz
${memgraph 30,300 00c2ff 11ecf7}
${color1}Mem$color $alignr$mem $memperc%
${color1}Load avg.$color $alignr${loadavg 1}, ${loadavg 2}, ${loadavg 3}
${color1}Uptime$color $alignr${uptime}
${color1}Power$color $alignr${acpiacadapter AC} $battery_short
${color1}Processes$color $alignr${color1}$running_processes$color / $processes
${color0}Sensors ${hr 1}
${color1}CPU:$color$alignr${hwmon k10temp temp 1}°C
${color1}GPU:$color$alignr${hwmon amdgpu temp 1}°C
${color1}NVMe:$color$alignr${hwmon nvme temp 1}°C
${color1}Fan:$color$alignr${hwmon thinkpad fan 1}, ${hwmon thinkpad fan 2}
]]

text = text .. [[
${color0}Disk ${hr 3}
$color1/$color $alignr${fs_used /} / ${fs_size /} ${fs_bar /}
${color1}Read$color $alignr$diskio_read ${diskiograph_read 10,120 00c2ff 11ecf7}
${color1}Write$color $alignr$diskio_write ${diskiograph_write 10,120 00c2ff 11ecf7}
${color0}Network ${hr 3}
${color1}Gateway$color $alignr$gw_ip dev $gw_iface$color0
${if_up <ethif>}Wired ${hr 1}
$alignr${addr <ethif>}
${color1}Rx speed$color $alignr${downspeed <ethif>} ${downspeedgraph <ethif> 10,120 00c2ff 11ecf7}
${color1}Tx speed$color $alignr${upspeed <ethif>} ${upspeedgraph <ethif> 10,120 00c2ff 11ecf7}
${color1}Total Rx$color $alignr${totaldown <ethif>}
${color1}Total Tx$color $alignr${totalup <ethif>}
$endif$color0${if_up <wlanif>}${if_running hostapd}Hotspot ${hr 1}${else}Wireless [CH ${wireless_channel <wlanif>}][${wireless_freq <wlanif>}] ${hr 1}
$alignr${addr <wlanif>}
${color1}ESSID$color $alignr${wireless_essid <wlanif>}
${color1}Link speed$color $alignr${wireless_bitrate <wlanif>}
${color1}Link quality$color $alignr${wireless_link_qual_perc <wlanif>}% ${wireless_link_bar <wlanif>}$endif
${color1}Rx speed$color $alignr${downspeed <wlanif>} ${downspeedgraph <wlanif> 10,120 00c2ff 11ecf7}
${color1}Tx speed$color $alignr${upspeed <wlanif>} ${upspeedgraph <wlanif> 10,120 00c2ff 11ecf7}
${color1}Total Rx$color $alignr${totaldown <wlanif>}
${color1}Total Tx$color $alignr${totalup <wlanif>}
$endif]];

text = string.gsub(text, "<wlanif>", wlanif)
text = string.gsub(text, "<ethif>", ethif)

conky.text = text

lfs = nil
